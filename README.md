# vim custom config #

Create a ~/.vim folder and git clone there, that's it.

* Uses Pathogen to manage plugins
* Using submodules for the plugins
  * Dont forget to git submodule init and git submodule update --init --recursive
* I'll try not to overcomplicate/clutter things
