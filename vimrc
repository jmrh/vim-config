" Pathogen
execute pathogen#infect()

" Colors and Syntax
syntax on
colorscheme delek

" Searches
set ignorecase
set smartcase
set hlsearch " highlight
set incsearch " incremental search

" Tabs
set smarttab 
set expandtab " spaces instead of tab 
set tabstop=3
set shiftwidth=3
set softtabstop=3 " for deletes
set autoindent

filetype plugin indent on

" Wildmenu
set wildmenu 

"  PLUGINS 
" Salt vim syntax
set nocompatible

" Nerdtree 

   " Remap open NerdTree to F2 key
map <F2> :NERDTreeToggle<CR>

   " Open NerdTree when when vim starts up with  no files specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

   " Close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
